import {operations} from '../src/arithmetic.js';

describe('Arithmetic Operations', ()=>{
  it('Should Add numbers.', ()=> {
    expect(operations.add(1,1)).toEqual(2);
  });
});