import {operations} from './arithmetic.js';

let result = operations.add(1, 1);
console.log(result);

result = operations.subtract(1, 1);
console.log(result);

var included = "abcd".includes("cd");
console.log(included);

let promise = new Promise((resolve, reject)=>{

});

//polyfill is a type of shim that retrofits legacy browsers with modern functionality
//shim is just another name for a polyfill
